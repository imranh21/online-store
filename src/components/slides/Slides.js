import React, { useState, useEffect, useContext } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination } from "swiper";
import "swiper/swiper.scss";
import { productContext } from "../../context/ProductContext";
import "./Slide.css";

SwiperCore.use([Navigation, Pagination]);
// import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";

// import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
const Slides = () => {
  const [slide, setSlide] = useState(0);
  const { allProducts } = useContext(productContext);

  return (
    <div className="section">
      <div className="container">
        <Swiper navigation className="slider">
          {allProducts.map((product) => {
            return (
              <SwiperSlide className="slide">
                <img src={product.image} />
              </SwiperSlide>
            );
          })}
        </Swiper>

        {/* <div
          className="containerImgBox"
          style={{ backgroundImage: `url(${allProducts[slide].image})` }}
        >
          <div className="left">
            <ArrowBackIosIcon
              onClick={() => slide > 0 && setSlide(slide - 1)}
            />
          </div>
          <div className="right">
            <ArrowForwardIosIcon
              onClick={() =>
                slide < allProducts.length - 1 && setSlide(slide + 1)
              }
            />
          </div>
        </div> */}
      </div>
    </div>
  );
};

export default Slides;
