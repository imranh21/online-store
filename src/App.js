import React from "react";
import { BrowserRouter } from "react-router-dom";
import ContextProvide from "./context/ProductContext";

import "./styles.css";
import Navbar from "./components/Navbar";
import Router from "./components/Router";

export default function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <ContextProvide>
          <Navbar />
          <Router />
        </ContextProvide>
      </BrowserRouter>
    </div>
  );
}
